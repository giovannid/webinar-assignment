import { render, screen } from '@testing-library/react';
import App from './App';

test('renders video player successfully', () => {
    render(<App />);
    const videoPlayer = screen.getByTestId('videoPlayer');
    expect(videoPlayer).toBeInTheDocument();
});
