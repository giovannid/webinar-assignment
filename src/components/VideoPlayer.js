import React, { useState, useRef, useEffect } from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
    @media (min-width: 768px) {
        width: 100%;
    }

    @media (min-width: 1200px) {
        width: 640px;
    }
    margin: 0 auto;
    position: relative;
`;

const Player = styled.video`
    width: 100%;
    @media (min-width: 1200px) {
        height: 480px;
    }
    background: #1a1a1a;
`;

const Button = styled.button`
    border: none;
    padding: 20px 25px;
    cursor: pointer;
    background: ${props => (props.enabled ? 'lightred' : 'lightgray')};
    @media (max-width: 576px) {
        width: 100%;
    }
`;

const Controls = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    width: 100%;
`;

const ErrorOverlay = styled.div`
    position: absolute;
    color: #fff;
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    p {
        margin-bottom: 20px;
    }
    button {
        width: 100px;
    }
`;

export function VideoPlayer() {
    const [permissionError, setPermissionError] = useState(false);
    const [cameraOn, setCameraOn] = useState(false);
    const [filterOn, setFilterOn] = useState(false);

    const player = useRef();
    const streamRef = useRef();

    const handleCameraButton = () => {
        setCameraOn(!cameraOn);
    };

    const handleFilterButton = () => {
        setFilterOn(!filterOn);
    };

    const handlePermissionButton = async () => {
        setPermissionError(false);
    };

    useEffect(() => {
        if (cameraOn) {
            player.current.srcObject = streamRef.current;
        } else {
            player.current.srcObject = null;
        }

        if (filterOn) {
            player.current.style.webkitFilter = 'sepia(0.5)';
        } else {
            player.current.style.webkitFilter = '';
        }
    }, [cameraOn, filterOn]);

    useEffect(() => {
        const constraints = {
            audio: false,
            video: true,
        };
        const getPermission = async () => {
            try {
                streamRef.current = await navigator.mediaDevices.getUserMedia(
                    constraints
                );
                player.current.srcObject = streamRef.current;
                setCameraOn(true);
            } catch (e) {
                setCameraOn(false);
                if (e.name === 'NotAllowedError') {
                    setPermissionError(true);
                }
                console.log(e);
            }
        };
        getPermission();
    }, [permissionError]);
    return (
        <Wrapper data-testid='videoPlayer'>
            {permissionError ? (
                <ErrorOverlay>
                    <p>No Permission to access camera!</p>{' '}
                    <p>
                        Please allow your camera to be used for this site and
                        then click the button below.
                    </p>
                    <Button onClick={handlePermissionButton}>Try Again</Button>
                </ErrorOverlay>
            ) : null}

            <Player ref={player} autoPlay />
            <Controls>
                <Button onClick={handleCameraButton} enabled={filterOn}>
                    Camera {cameraOn ? 'Off' : 'On'}
                </Button>
                <Button onClick={handleFilterButton} enabled={cameraOn}>
                    Filter {filterOn ? 'Off' : 'On'}
                </Button>
            </Controls>
        </Wrapper>
    );
}
